ESCAPADE = (function() {

	var RSS = "http://escapade-carbet.com/feed";
	var actusFirstLaunch = false;
	var mapFirstLaunch = false;
	var lastMarkerMap = -1;
	
	/* displayCarbetList()
	Affiche la liste des carbets passés en paramètres
	Entrée : 	carbets_list (array) : tableau de carbets
	Sortie :	-
	*/
	function displayCarbetList(carbets_list) {
		console.log("Escapade Carbet Mobile - displayCarbetList : Affichage de la liste des carbets");
	
		var carbetsListHTML =  $('#carbetsList');
		carbetsListHTML.empty();
		
		// On trie le tableau par ordre alphabétique à partir du titre
		carbets_list.sort(function(a,b) {
			var titreA = a.nom, titreB = b.nom;
			if (titreA < titreB)
				return -1 ;
			if (titreA > titreB)
				return 1 ;
			return 0 ;
		} );
		
		for ( var i=0; i<carbets_list.length; i++ ) {
			var li = $( '<li><a href="#carbet-detail?id=' + carbets_list[i].id + '">' +
						' 	<h3>' + carbets_list[i].nom + '</h3>' +
						' 	<img src="' + carbets_list[i].miniature + '" />' +
						' 	<p>' + carbets_list[i].details_resume + '</p>' +
						' 	<p class="ui-li-aside"><strong>' + carbets_list[i].commune + '</strong></p>' +
						' 	<div class="meta">' +
						' 		<div class="meta_note"><div class="carbet_note_globale" style="width:' + (carbets_list[i].note_globale * 20) + '%"></div></div>' +
						' 		<div class="meta_prix">A partir de : ' + carbets_list[i].tarif + '</div>' +
						' 	</div>' +
						'</a><div class="li_bottom"></div></li>');
			carbetsListHTML.append( li );
			
			console.log("Escapade Carbet Mobile - displayCarbetList : carbet #" + i + " affiché");
		}
	}
	
	/* displayPageList()
	Affiche la liste des pages passées en paramètres
	Entrée : 	pages_list (array) : tableau de pages
	Sortie :	-
	*/
	function displayPageList(pages_list) {
		console.log("Escapade Carbet Mobile - displayPageList : Affichage de la liste des pages");
	
		var pagesListHTML =  $('#pagesList');
		pagesListHTML.empty();
		
		// On trie le tableau par ordre alphabétique à partir du titre
		pages_list.sort(function(a,b) {
			var titreA = a.nom, titreB = b.nom;
			if (titreA < titreB)
				return -1 ;
			if (titreA > titreB)
				return 1 ;
			return 0 ;
		} );
		
		for ( var i=0; i<pages_list.length; i++ ) {
			var li = $( '<li><a href="#page-detail?id=' + pages_list[i].id + '">' +
						' 	<h3>' + pages_list[i].titre + '</h3>' +
						' 	<p class="ui-li-desc">' + pages_list[i].sous_titre + '</p>' +
						'</a><div class="li_bottom"></div></li>');
			pagesListHTML.append( li );
			
			console.log("Escapade Carbet Mobile - displayPageList : page #" + i + " affichée");
		}
	}
	
	/* displayActuList()
	Affiche la liste des actus passées en paramètres
	Entrée : 	actus_list (array) : tableau de pages
	Sortie :	-
	*/
	function displayActuList(actus_list) {
		console.log("Escapade Carbet Mobile - displayActuList : Affichage de la liste des actus");
	
		var actusListHTML =  $('#actusList');
		
		actusListHTML.empty();
		actusListHTML.listview('refresh');
	
		if ((actus_list.length == 0) || (actus_list == null )) {
			$('#actu_status').html("Rien à afficher pour le moment :(");
			actusListHTML.listview('refresh');
		} else {
			
			// On trie le tableau par ordre anti-chronologique à partir du timeStamp Unix
			actus_list.sort(function(a,b) {return b.id - a.id ;} );
			
			for ( var i=0; i<actus_list.length; i++ ) {
				var li = $( '<li><a href="#actu-detail?id=' + actus_list[i].id + '">' +
							' 	<h3>' + actus_list[i].title + '</h3>' +
							' 	<p>' + actus_list[i].contentSnippet + '</p>' +
							' 	<p class="ui-li-aside"><strong>' + BITWIP.humanDate(actus_list[i].publishedDate) + '</strong></p>' +
							'</a><div class="li_bottom"></div></li>');
				actusListHTML.append( li );
				
				console.log("Escapade Carbet Mobile - displayActuList : actu #" + i + " affichée");
			}
			
			actusListHTML.listview('refresh')
			// Hack pour rajouter les bords arrondis en haut et en bas, en attendant un fix JQuery Mobile
			actusListHTML.find("li:first").addClass("ui-corner-top");
			actusListHTML.find("li:last").addClass("ui-corner-bottom");
			
		}
	}

	
	/* checkNewList(type)
	Vérifie si une version plus récente des données existe sur le serveur - A DEVELOPPER
	Entrée : 	type (string) : clé qui définit le type de donnée
	Sortie :	(boolean)
	*/
	function checkNewList(type) {
		return false;
	}

	/* updateList(type)
	Met à jour le localstorage avec les nouvelles données récupérées du serveur - A DEVELOPPER
	Entrée : 	type (string) : clé qui définit le type de donnée
	Sortie :	?
	*/
	function updateList() {
	/*
	- Vérifier si on a du réseau
		- Si OUI, appel API pour récupérer la version de la liste et vérification de la date
			- Si version locale < version API :
				- on télécharge la liste complète via l'API
				- on met à jour le localstorage
	*/
	}

	/* getList(type)
	Renvoie la liste des données stockées dans le localstorage
	Entrée : 	type (string) : clé qui définit le type de donnée
	Sortie :	list (array) : tableau des données stockées dans le localstorage
	*/
	function getList(type) {
	
		if (checkNewList(type)) {
			updateList(type);
		}
		
		if (window.localStorage.length - 1) {
            var list = [], i, key;
            for (i = 0; i < window.localStorage.length; i++) {
                key = window.localStorage.key(i);
                if (key.startsWith(type)) {
                    list.push(BITWIP.Storage.get(key));
                }
            }
        }
        
        return list;
	}
	
	/* getUrlId()
	Renvoie l'id qui se trouve comme paramètre dans une URL
	Entrée : 	-
	Sortie :	id (int) :
	*/
	function getUrlId() {
		// return BITWIP.getUrlVars()["id"];
		if ($.mobile.pageData && $.mobile.pageData.id) 
			return $.mobile.pageData.id;
		else 
			return -1;
	}

	
	/* displayCarbetDetails()
	Affiche les infos complètes d'un carbet passé en paramètre
	Entrée : 	id (integer) : identifiant du carbet 
	Sortie :	-
	*/
	function displayCarbetDetails(id) {
		$.mobile.showPageLoadingMsg();
		
		$('#carbet_btn_album').show();
		$('#carbet_btn_tel1').show();
		
		var carbet = BITWIP.Storage.get("carbet:" + id);
		var carbet_photo;
		if (carbet.photos.length > 0) carbet_photo = carbet.photos[0].url ; 
			else { 
				carbet_photo = 'img/logo_Escapade_Carbet_blank.png';
				$('#carbet_btn_album').hide();
			}
		if (carbet.tel1 == "Inconnu") $('#carbet_btn_tel1').hide();
		
		$('#carbet_nom').html(carbet.nom);
		$('#carbet_album_solo').attr("src",carbet_photo);
		$('#carbet_link_album_solo').attr("href","#carbet-album?id=" + carbet.id);
		$('#carbet_btn_album .ui-btn-text').html("Voir l'album (" + carbet.photos.length + " photos)");
		$('#carbet_btn_album').attr("href","#carbet-album?id=" + carbet.id);
		$('#carbet_btn_tel1 .ui-btn-text').html("Appeler : " + carbet.tel1 );
		$('#carbet_btn_tel1').attr("href", "tel:"+carbet.tel1.split('.').join(''));
		$('#carbet_btn_carte').attr("href", "#carbet-carte?id=" + carbet.id);
		console.log("Escapade Carbet Mobile - #carbet_btn_carte / carbet.id = " + carbet.id);
		$('#carbet_details_resume').html(carbet.details_resume);
		
		$('#carbet_commune').html("<strong>Commune : </strong>" + carbet.commune );
		$('#carbet_nbre_places').html("<strong>Places hamacs : </strong>" + carbet.nbre_places );
		$('#carbet_fleuve').html("<strong>Fleuve : </strong>" + carbet.fleuve );
		$('#carbet_tarif').html("<strong>Gamme de prix : </strong>" + carbet.tarif );
		//$('#carbet_type').html("<strong>Type : </strong" + carbet.type );
		
		for (i = 0; i < carbet.services.length; i++) {
            var span = $('<span class="picto_service"><img src="img/icones/' + carbet.services[i].slug +'.png"/></span>');
        	$('#carbet_services').append(span);
		}
		
		$('#carbet_details_complet').html(carbet.details_complet);
		$('#carbet_details_tarif').html(carbet.details_tarif);
		$('#carbet_details_trajet').html(carbet.details_trajet);
		$('#carbet_details_autour').html(carbet.details_autour);
		
		$.mobile.hidePageLoadingMsg();
		
		console.log("Escapade Carbet Mobile - displayCarbetDetails : carbet '" + carbet.nom + "' affiché");
	}

	
	/* displayPageDetails(id)
	Affiche les infos complètes d'une page passée en paramètre
	Entrée : 	id (integer) : identifiant de la page 
	Sortie :	-
	*/
	function displayPageDetails(id) {
		$.mobile.showPageLoadingMsg();

		var page = BITWIP.Storage.get("page:" + id);
		
		$('#page_titre').html(page.titre);
		$('#page_contenu').html(page.contenu).trigger('create');

		$.mobile.hidePageLoadingMsg();
		
		console.log("Escapade Carbet Mobile - displayPageDetails : page '" + page.titre + "' affichée");
	}
	
	/* displayActuDetails(id)
	Affiche les infos complètes d'une actu passée en paramètre
	Entrée : 	id (integer) : identifiant de l'actu 
	Sortie :	-
	*/
	function displayActuDetails(id) {
		$.mobile.showPageLoadingMsg();

		var actu = BITWIP.Storage.get("actu:" + id);
		// On isole le type d'actu
		var actu_type = actu.title;
		var reg = new RegExp("[:]+", "g");
		var actu_type_array = actu_type.split(reg);
		
		$('#actu_type').html(actu_type_array[0]);
		$('#actu_titre').html(actu.title);
		$('#actu_date').html("Publié le : " + BITWIP.humanDate(actu.publishedDate));
		$('#actu_contenu').html(actu.content).trigger('create');

		$.mobile.hidePageLoadingMsg();
		
		console.log("Escapade Carbet Mobile - displayActuDetails : actu '" + actu.title + "' affichée");
	}
	
	/* displayAlbumDetails(id)
	Affiche l'album photo d'un carbet passé en paramètre
	Entrée : 	id (integer) : identifiant du carbet 
	Sortie :	-
	*/
	function displayAlbumDetails(id) {
		$.mobile.showPageLoadingMsg();
		
		var carbet = BITWIP.Storage.get("carbet:" + id);
		
		$('#album_back').attr("href","#carbet-detail?id=" + id);
		$('#album_nom').html(carbet.nom);
		
		for (i = 0; i < carbet.photos.length; i++) {
            var li = $('<li><a href="' + carbet.photos[i].url + '" rel="external"><img src="' + carbet.photos[i].url + '" alt="' + carbet.photos[i].legende + '" /></a></li>');
        	$('#album_galerie').append(li);
		}
		
		$.mobile.hidePageLoadingMsg();
		
		console.log("Escapade Carbet Mobile - displayAlbumDetails : allbum de '" + carbet.nom + "' affiché");
	}
	
	/* resetCarbetDetails()
	Réinitialise la page de gabarit d'un carbet
	Entrée : 	-
	Sortie :	-
	*/
	function resetCarbetDetails() {
		$('#carbet_nom').html("");
		$('#carbet_album_solo').attr("src","");
		$('#carbet_link_album_solo').attr("href","");
		$('#carbet_btn_album .ui-btn-text').html("");
		$('#carbet_btn_album').attr("href","");
		$('#carbet_btn_tel1 .ui-btn-text').html("");
		$('#carbet_btn_tel1').attr("href", "");
		$('#carbet_details_resume').html("");
		$('#carbet_nbre_places').html("" );
		$('#carbet_fleuve').html("");
		$('#carbet_tarif').html("");
		$('#carbet_type').html("");
		$('#carbet_services').html("");
		$('#carbet_details_complet').html("");
		$('#carbet_details_tarif').html("");
		$('#carbet_details_trajet').html("");
		$('#carbet_details_autour').html("");
		
		//console.log("Escapade Carbet Mobile - resetCarbetDetails : carbet réinitialisé");
	}
	
	/* resetPagetDetails()
	Réinitialise les infos complètes du gabarit de page
	Entrée : 	- 
	Sortie :	-
	*/
	function resetPageDetails() {	
		$('#page_titre').html("");
		$('#page_contenu').html("");
		
		console.log("Escapade Carbet Mobile - resetPageDetails : page réinitialisée");
	}
	
	/* resetActuDetails()
	Réinitialise les infos complètes du gabarit de l'actu
	Entrée : 	- 
	Sortie :	-
	*/
	function resetActuDetails() {	
		$('#actu_titre').html("");
		$('#actu_contenu').html("");
		//$('#actu_status').html("");
		
		console.log("Escapade Carbet Mobile - resetActuDetails : actu réinitialisée");
	}
	
	/* resetAlbumDetails()
	Réinitialise la page de l'album photo d'un carbet
	Entrée : 	-
	Sortie :	-
	*/
	function resetAlbumDetails() {
		$('#album_nom').html("");
		$('#album_galerie').html("");
		
		console.log("Escapade Carbet Mobile - resetAlbumDetails : album réinitialisé");
	}
	
	/* isOnline()
	Vérifie si on a du réseau ou accès à Internet
	Entrée : 	-
	Sortie :	true/false
	*/
	function isOnline() {
	
		if (!BITWIP.isPhonegap()) {
			console.log("Escapade Carbet Mobile - isOnline : ONLINE IN BROWSER");
			return true;
		}
	
		var networkState = navigator.network.connection.type;		
	    var states = {};

	    states[Connection.UNKNOWN]  = 'offline';
	    states[Connection.ETHERNET] = 'online';
	    states[Connection.WIFI]     = 'online';
	    states[Connection.CELL_2G]  = 'online';
	    states[Connection.CELL_3G]  = 'online';
	    states[Connection.CELL_4G]  = 'online';
	    states[Connection.NONE]     = 'offline';
	    
	    if (states[networkState] === 'offline') {
	    	console.log("Escapade Carbet Mobile - isOnline : OFFLINE");
	    	return false;
	    } else {
	    	console.log("Escapade Carbet Mobile - isOnline : ONLINE");
	    	return true;
	    }
	}
	
	/* getIdCarbetFromName()
	Renvoie l'id d'un carbet à partir de son nom (utile pour la carto)
	Entrée : 	nomCarbet (String) : nom du carbet
	Sortie :	id (Interger) : identifiant unique du carbet tel que définit dans la base
	*/
	function getIdCarbetFromName(nomCarbet) {
		if (window.localStorage.length - 1) {
            var list = [], i, key;
            for (i = 0; i < window.localStorage.length; i++) {
                key = window.localStorage.key(i);
                if (key.startsWith("carbet:")) {
                    var tempValue = BITWIP.Storage.get(key);
                    if (tempValue.nom == nomCarbet)
                    	return tempValue.id;
                }
            }
        }
        // Si on n'a pas trouvé le nom, on renvoie un code d'erreur
        return -1;
	}
	
	/* getCarbetIdFromMarkerId()
	A FAIRE : Rendre générique cette fonction accepter un type en paramètre
	Renvoie l'id d'un carbet à partir d'un identifiant de marker (utile pour la carto)
	Entrée : 	markerId (Int) : identifiant du marker
	Sortie :	id (Interger) : identifiant unique du carbet tel que définit dans la base
	*/
	function getCarbetIdFromMarkerId(markerId) {
		//alert('getCarbetIdFromMarkerId - markerId = ' + markerId);
	
		var marker = $('#map_canvas').gmap('get', 'markers')[markerId];
		//alert('Marker = ' + marker.title);
		
		if (window.localStorage.length - 1) {
            var list = [], i, key;
            for (i = 0; i < window.localStorage.length; i++) {
                key = window.localStorage.key(i);
                if (key.startsWith("carbet:")) {
                    var tempValue = BITWIP.Storage.get(key);
                    if (tempValue.nom == marker.title)
                    	return tempValue.id;
                }
            }
        }
        // Si on n'a pas trouvé le nom, on renvoie un code d'erreur
        return -1;
	}
	
	/* getMarkerIdFromPlaceId()
	Renvoie l'id d'un marker à partir d'un identifiant de carbet (utile pour la carto)
	Entrée : 	carbetId (Int) : identifiant du carbet
	Sortie :	id (Interger) : identifiant unique du marker
	*/
	function getMarkerIdFromPlaceId(placeId) {
	
		// A FAIRE = ajouter "type" en paramètre ou trouver un moyen d'être + générique qu'un carbet
	
		var carbet = BITWIP.Storage.get("carbet:" + placeId)
		var markers = $('#map_canvas').gmap('get', 'markers');
		
		for (var i = 0; i < markers.length; i ++) {
			//alert("Nom marker #" + i + " = '" + markers[i].title + "' - carbet = '" + carbet.nom + "'");
			// A FAIRE : faire fonctionner ce …. de test
			if ((markers[i].title) === (carbet.nom)) {
				//alert("STOOP : i = " + i);
				console.log("Escapade Carbet Mobile - getMarkerIdFromPlaceId : Marker Id = " + i);	
				return i;
			}
		}
		
        // Si on n'a pas trouvé le nom, on renvoie un code d'erreur
        console.log("Escapade Carbet Mobile - getMarkerIdFromPlaceId : Aucun Marker Id trouvé :(");	
		return -1;
	}

	
	/* setListeners()
	Initialise les listeners sur les pages du site pour déclencher les fonctions
	Entrée : 	-
	Sortie :	-
	*/
	function setListeners() {	

		// Génère la page de la liste des carbets
		$('#carbet-liste').live('pagebeforeshow', function(event) {
			console.log("Escapade Carbet Mobile - #carbet-liste event : pageshow");	
		});
		
		// Génère la page de la carto
		$('#carbet-carte').live('pageshow', function(event) {
			console.log("Escapade Carbet Mobile - #carbet-carte event : pageshow");	

			if (isOnline()) {
				// Redimensionne le canvas de la carto pour corriger un bug d'affichage (carte collée en haut à droite de l'écran)
				resizeMap();
		
				$("#pagination").show();	
				
				var placeId = getUrlId();
				console.log("Escapade Carbet Mobile - setListeners / placeId = " + placeId);				
				displayMap(placeId);
				
				//$('#map_canvas').gmap('refresh');			
				
			} else {
				$("#pagination").hide();
				$("#map_canvas").html("<div class='erreur'>Vous n'êtes pas connecté à Internet :( Réessayez quand vous aurez du réseau.</div>");
			}
		});
		
		// Si on change l'orientation du téléphone, la carte se redessine pour remplir l'écran
		//window.addEventListener("orientationchange resize", resizeMap, false);
		
		// Génère la page du détail d'un carbet
		$('#carbet-detail').live('pagebeforeshow', function(event) {
			console.log("Escapade Carbet Mobile - #carbet-detail EVENT : pagebeforeshow");	
			var id = getUrlId();
			// A FAIRE : if (id == null) {id = BITWIP.Storage.get("meta:carbets-last-id")}
			displayCarbetDetails(id);
		});
		
		// Efface la page du détail d'un carbet
		$('#carbet-detail').live('pagehide', function(event) {
			console.log("Escapade Carbet Mobile - #carbet-detail EVENT : pagehide");
			resetCarbetDetails();
		});
		
		// Génère la page de la liste des pages de contenu
		$('#page-liste').live('pageshow', function(event) {
			console.log("Escapade Carbet Mobile -#page-liste EVENT : pageshow");
		});
		
		// Génère la page du détail d'une page de contenu
		$('#page-detail').live('pagebeforeshow', function(event) {
			console.log("Escapade Carbet Mobile - #page-detail EVENT : pagebeforeshow");
			var id = getUrlId();
			// A FAIRE : if (id == null) {id = BITWIP.Storage.get("meta:carbets-last-id")}
			displayPageDetails(id);
		});
		
		// Efface la page du détail d'une page de contenu
		$('#page-detail').live('pagehide', function(event) {
			console.log("Escapade Carbet Mobile - #page-detail EVENT : pagehide");
			resetPageDetails();
		});
		
		// Génère la page de l'album photos d'un carbet
		$('#carbet-album').live('pagebeforeshow', function(event) {
			console.log("Escapade Carbet Mobile - #carbet-album EVENT : pagebeforeshow");
			var id = getUrlId();
			// A FAIRE : if (id == null) {id = BITWIP.Storage.get("meta:carbets-last-id")}
			displayAlbumDetails(id);
		});
		
		// Efface la page de l'album photos d'un carbet
		$('#carbet-album').live('pagehide', function(event) {
			console.log("Escapade Carbet Mobile - #carbet-album EVENT : pagehide");
			resetAlbumDetails();
		});
		
		// Récupère le flux RSS ds actus quand on charge la page des actus
		$('#actu-liste').live('pageinit', function(event) {
			console.log("Escapade Carbet Mobile - #actu-liste EVENT : pageinit");
			
		});
		
		// Récupère le flux RSS ds actus quand on charge la page des actus
		$('#actu-liste').live('pageshow', function(event) {
			console.log("Escapade Carbet Mobile - #actu-liste EVENT : pageshow");
			if (!actusFirstLaunch && isOnline()) {
				$.mobile.showPageLoadingMsg("a","Actualisation des actus en cours…");
			}
		});
		
		// Génère la page de la liste des actus
		$('#actu-liste').live('pagebeforeshow', function(event) {
			console.log("Escapade Carbet Mobile - #actu-liste EVENT : pagebeforeshow");
			if (isOnline()) {
				$("#actu_refresh").removeClass('disabled');
				
				initRSS();
				displayActuList(getList("actu"));
			} else {
				$("#actu_refresh").addClass('disabled');
				
				var tempList = getList("actu");
				if (tempList.length > 0) {
					$('#actu_status').html("Mode hors-connexion. Vous lisez les actus stockées en cache. Désolé, pas d'images pour le moment :(");
					displayActuList(getList("actu"));
				} else {
					$('#actu_status').html("Aucune actu à afficher. Etes-vous connecté à Internet ?");
				}
			}
			
		});
		
		// Rafraichit la liste des actus quand le bouton "Actualiser" est cliqué
		$( "#actu_refresh" ).bind( "click", function(event, ui) {
			console.log("Escapade Carbet Mobile - #actu_refresh EVENT : click");
 			if (isOnline()) {
	 			$.mobile.showPageLoadingMsg("a","Actualisation des actus en cours…");
	 			actusFirstLaunch = false;
				initRSS();
			} else {
				// A FAIRE : Remplacer par une fenêtre modale plus friendly - data-role=dialog
				alert("Vous êtes hors-ligne :( Réessayez quand vous aurez du réseau.");
			}
		});
		
		// Génère la page du détail d'une actu
		$('#actu-detail').live('pagebeforeshow', function(event) {
			console.log("Escapade Carbet Mobile - #actu-detail EVENT : pagebeforeshow");
			var id = getUrlId();
			// A FAIRE : if (id == null) {id = BITWIP.Storage.get("meta:carbets-last-id")}
			displayActuDetails(id);
		}); 
		
		
		// Efface la page du détail d'une actu
		$('#actu-detail').live('pagehide', function(event) {
			console.log("Escapade Carbet Mobile - #actu-detail EVENT : pagehide");
			resetActuDetails();
		});
		
		
		// Init Album
		(function(window, $, PhotoSwipe){
			$(document).ready(function(){			
				$('div.gallery-page')
					.live('pageshow', function(e){
						var 
							currentPage = $(e.target),
							options = {allowUserZoom: false},
							photoSwipeInstance = $("ul.gallery a", e.target).photoSwipe(options,  currentPage.attr('id'));
						return true;
					})		
					.live('pagehide', function(e){
						var 
							currentPage = $(e.target),
							photoSwipeInstance = PhotoSwipe.getInstance(currentPage.attr('id'));

						if (typeof photoSwipeInstance != "undefined" && photoSwipeInstance != null) {
							PhotoSwipe.detatch(photoSwipeInstance);
						}
						return true;	
					});	
			});
		}(window, window.jQuery, window.Code.PhotoSwipe));
		
		document.addEventListener("pause", onPause, false);
		function onPause() {
    		console.log("Escapade Carbet Mobile - pause EVENT");
		}
		
		document.addEventListener("resume", onResume, false);
		function onResume() {
    		console.log("Escapade Carbet Mobile - resume EVENT");
		}
		
		console.log("Escapade Carbet Mobile - setListeners : Init des listeners terminé");
	}
	
	/* initLocalStorage()
	Initialise le localStorage s'il est vide au premier lancement de l'application
	Entrée : 	-
	Sortie :	-
	*/
	function initLocalStorage() {
		// DEBUG : On nettoie le localStorage
		//BITWIP.Storage.clear();
		//console.log("Escapade Carbet Mobile - initLocalStorage : Local Storage réinitialisé");
	
		// On teste si le localstorage a déjà initialisé. Si non, on le remplit avec les valeurs par défaut.
		if (window.localStorage.length == 0) {
			// Chargement de la base des carbets
			for ( var i=0; i<carbets.length; i++ ) {
				BITWIP.Storage.set("carbet:"+carbets[i].id,carbets[i]);
			}
			BITWIP.Storage.set("meta:carbets-last-update", CARBETS_LAST_UPDATE);
			console.log("Escapade Carbet Mobile - initLocalStorage : données des carbets chargées");
			
			// Chargement de la base des pages
			for ( var i=0; i<pages.length; i++ ) {
				BITWIP.Storage.set("page:" + pages[i].id, pages[i]);
			}
			BITWIP.Storage.set("meta:pages-last-update", PAGES_LAST_UPDATE);
			console.log("Escapade Carbet Mobile - initLocalStorage : données des pages chargées");
		}
	}
	
	/* initRSS()
	Initialise le flux d'actualités à partir d'un flux RSS
	http://www.raymondcamden.com/index.cfm/2012/1/24/PhoneGap-RSS-Reader--Part-3
	Entrée : 	-
	Sortie :	entries (Array) : contient le flux RSS
	*/
	function initRSS() {
	
		if (!actusFirstLaunch) {
		
			// Create a feed instance that will grab Digg's feed.
			var feed = new google.feeds.Feed(RSS);
			feed.setNumEntries(10);
			feed.setResultFormat(google.feeds.Feed.JSON);
	
	  		// Calling load sends the request off.  It requires a callback function.
			feed.load(function(result) {
				 if (!result.error) { 
				 	console.log("Escapade Carbet Mobile - initRSS : Google Feed API / Flux OK");
				 
				 	for (var i = 0; i < result.feed.entries.length; i++) {
				 		var entry = result.feed.entries[i];
				 		var timeStamp = Math.round((new Date(entry.publishedDate)).getTime() / 1000);
						entry.id = timeStamp;		
						
						BITWIP.Storage.set("actu:" + timeStamp, entry);
						console.log("Escapade Carbet Mobile - initRSS : actu #" + timeStamp + " ajoutée");
				 	}
				 	
				 	$('#actu_status').html("Vous êtes en ligne. Cliquez sur 'Actualisez' pour rafraîchir la liste des actus.");
					displayActuList(getList("actu"));
					actusFirstLaunch = true;
					$.mobile.hidePageLoadingMsg();
				 	
				 } else {
				 	console.log("Escapade Carbet Mobile - initRSS : Google Feed API / Erreur");
				 	$('#actu_status').html("Erreur lors de la récupération du flux RSS");
					actusFirstLaunch = true;
					$.mobile.hidePageLoadingMsg();
				 }
			
			});
		} else {
		
		}			
	}
	
	/* resizeMap()
	Redimensionne le Canvas de la carte pour s'adapter à l'acran
	Entrée : 	-
	Sortie :	-
	*/
	function resizeMap() {
		window.setTimeout(function() {
		
			if (window.orientation == 90 || window.orientation == 270) {
				$('meta[name="viewport"]').attr('content', 'height=device-width,width=device-height,initial-scale=1.0,maximum-scale=1.0'); } else {
				$('meta[name="viewport"]').attr('content', 'height=device-height,width=device-width,initial-scale=1.0,maximum-scale=1.0');
			}
			
			$("#map_canvas").width( $(window).width() );
			$("#map_canvas").height(  
	            	 (window.innerHeight ? window.innerHeight : $(window).height())
	            	- $("#carbet-carte header.ui-header").outerHeight(true)    
	        );
		},0);
		window.setTimeout(function() { $('#map_canvas').gmap('refresh');}, 0);
	}
	
	/* updatePagination()
	Actualise le cartouche de pagination sur la page de la carte (nom du carbet + méta données)
	Entrée : 	markerId (Int) : identifiant du marqueur associé au point à mettre en valeur
	Sortie :	-
	*/
	function updatePagination (markerId) {
		
		// Si le placeId correspond à une valeur réelle (autre que valeur d'init)
		if (markerId >= 0) {
			
			// On sauvegarde le marqueur
			lastMarkerMap = markerId;
		
			// A FAIRE : Réinitiliser la bonne image en cas de picto annuaire
	   		// Utiliser le tag du marker
			var markerImageActive = new google.maps.MarkerImage('img/picto_map/carbet_active.png',
				new google.maps.Size(32, 37),
				new google.maps.Point(0,0),
				new google.maps.Point(0,0));
			var markerImage = new google.maps.MarkerImage('img/picto_map/carbet.png',
				new google.maps.Size(32, 37),
				new google.maps.Point(0,0),
				new google.maps.Point(0,0));

			// On met à jour les images des marqueus
			var markers = $('#map_canvas').gmap('get', 'markers');
			for (var i=0; i<markers.length; i++) {
				if (markers[i].id == markerId) {
					markers[i].setIcon(markerImageActive);
					$('#map_canvas').gmap('option', 'zoom', 12);
					$('#map_canvas').gmap('get', 'map').setCenter(markers[i].position);
					$('#map_canvas').gmap('refresh');
				} else
					markers[i].setIcon(markerImage);
			}
			
			placeId = getCarbetIdFromMarkerId(markerId);
			var carbet = BITWIP.Storage.get("carbet:" + placeId);
			
			$('#pagination .display a').attr('href', '#carbet-detail?id=' + placeId);
			$('#pagination .nom').html('<a href="#carbet-detail?id=' + placeId + '">' + carbet.nom + '</a>');
			$('#pagination .meta_note').show();
			$('#pagination .carbet_note_globale').css("width",carbet.note_globale * 20 + "%");
			$('#pagination .carbet_nbre_avis').html(carbet.nbre_avis + " avis");
			$('#pagination .meta_commune').html(carbet.commune);
						
			$('#pagination .display').attr('data-id',markerId);
			
		} else {
			// sinon (valeur d'init < 0)
			$('#pagination .display a').attr('href', '#');
			$('#pagination .nom').html("<em>Cliquez sur un carbet</em>");
			$('#pagination .meta_note').hide();
			$('#pagination .carbet_nbre_avis').html("");
			$('#pagination .meta_commune').html("");
		}
		
		//window.setTimeout(function() { $('#map_canvas').gmap('refresh');}, 100);
	
	}
	
	/* displayMarkersType()
	Affiche l'ensemble des markets pour un type de donnée
	Entrée : 	type (String) : type de données à afficher (ex: 'carbet')
				offset (Int) : Nombre de markers déclarés avant l'appel de la fonction, pour ne pas casser la barre de navigation
	Sortie :	-
	*/
	function displayMarkersType(type, offset) {
			
		var data = getList(type);
		
		var markerImage = new google.maps.MarkerImage('img/picto_map/'+type+'.png',
			new google.maps.Size(32, 37),
			new google.maps.Point(0,0),
			new google.maps.Point(0,0));
		var markerImageShadow = new google.maps.MarkerImage('img/picto_map/shadow.png',
			new google.maps.Size(54, 37),
			new google.maps.Point(0,0),
			new google.maps.Point(0,0));
		
		for (var i = 0; i < data.length; i ++) {
					
			var $marker = $('#map_canvas').gmap('addMarker', { 
				id: i+offset,
				'position': new google.maps.LatLng(data[i].latitude, data[i].longitude), 
				'bounds': true,
				'title' : data[i].nom,
				'icon' : markerImage,
				'shadow' : markerImageShadow,
				'tags': type
			});
						
			$marker.click(function() {
				updatePagination(this.id);
			});

		}
		
	}

	
	/* displayMap()
	Initialise la carto
	Entrée : 	placeId (Int) : identifiant du lieu associé au point à mettre en valeur
	Sortie :	-
	*/
	function displayMap(placeId) {
	
		// Si la carte n'a pas été initialisée, on la charge
		if (!mapFirstLaunch) {
		
			$("#map_canvas").width( $(window).width() );
			$("#map_canvas").height(  
	            	 (window.innerHeight ? window.innerHeight : $(window).height())
	            	- $("#carbet-carte header.ui-header").outerHeight(true)   
	        );
			
			var guyaneLatLng = new google.maps.LatLng( 4.4942854907702205, -53.118896484375);
/*
			// Informations sur l'image du marqueur "Vous êtes ici"
			var markerImageHere = new google.maps.MarkerImage('img/picto_map/here.png',
				new google.maps.Size(32, 37),
				new google.maps.Point(0,0),
				new google.maps.Point(0,0));
			var markerImageShadow = new google.maps.MarkerImage('img/picto_map/shadow.png',
				new google.maps.Size(54, 37),
				new google.maps.Point(0,0),
				new google.maps.Point(0,0));
*/
			
			$('#map_canvas').gmap(
				{	'center': guyaneLatLng,
					'zoomControlOptions': {'position':google.maps.ControlPosition.LEFT_TOP},
					'streetViewControl' : false,
					'mapTypeId': google.maps.MapTypeId.TERRAIN
				}).
				bind('init', function(evt, map) {
					console.log("Escapade Carbet Mobile - displayMap / Carte initialisée");
					
					/*
$.mobile.showPageLoadingMsg("c","Nous essayons de vous g&eacute;olocaliser…");
					
		            $('#map_canvas').gmap('getCurrentPosition', function(position, status) {
		                if (status === "OK") {
		                
		                    var clientPosition = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
		                    $('#map_canvas').gmap('addMarker', {
		                    	'position': clientPosition, 
		                    	'bounds': false,
		                    	'title': '<em>Votre position actuelle</em>',
		                    	'tags': 'here',
		                    	'clickable': false,
  								'icon': markerImageHere,
								'shadow' : markerImageShadow
		                    });

							$('#map_canvas').gmap('addShape', 'Circle', { 
								'strokeWeight': 0, 
								'fillColor': "#008595", 
								'fillOpacity': 0.25, 
								'center': clientPosition, 
								'radius': position.coords.accuracy, 
								'clickable': false 
							});

							$('#map_canvas').gmap('option', 'center', clientPosition);
							$('#map_canvas').gmap('option', 'zoom', 7);
							
							displayMarkersType('carbet', 1);
							$('#map_canvas').gmap('pagination' , 'title');	
							$.mobile.hidePageLoadingMsg();
							// On initialise la pagination avec le 1er élément (dans ce cas, l'élément 0 = "vous êtes ici")
							updatePagination(-1);
							
		                } else {
		                
							displayMarkersType('carbet', 0);
							$('#map_canvas').gmap('pagination' , 'title');
							$.mobile.hidePageLoadingMsg();
							updatePagination(-1);
		                }                   
		            }, {timeout: 10000 });
*/

					$('#map_canvas').gmap('addControl', $("#pagination"), google.maps.ControlPosition.BOTTOM_CENTER);
					
		            // Affichage des marqueurs
		            displayMarkersType('carbet', 0);
		            
		            // Création de la pagination
		            //callPagination(placeId);
		            // $('#map_canvas').gmap('pagination' , 'title');
		            
		            if (placeId >= 0) {
						var markerId = getMarkerIdFromPlaceId(placeId);
						$('#pagination .display').attr('data-id',markerId);
						updatePagination(markerId);
					}
					
					// Léger tempo avant de rafraichir sinon carte pas fini de redessiner
					window.setTimeout(function() { $('#map_canvas').gmap('refresh');}, 100);
					
				}).gmap('pagination' , 'title');         
			
			mapFirstLaunch = true;		

		} else {
			// La carto a déjà été initialisée une fois
			
			if (lastMarkerMap == -1) {
				$('#pagination .display').attr('data-id',-100);
			}
				
			if (placeId >= 0) {
				var markerId = getMarkerIdFromPlaceId(placeId);
				$('#pagination .display').attr('data-id',markerId);
				updatePagination(markerId);
			}
			
			// Léger tempo avant de rafraichir sinon carte pas fini de redessiner
			window.setTimeout(function() { $('#map_canvas').gmap('refresh');}, 100);
		}

	}
	
	/* callPagination(placeId)
	Fonction utilitaire pour éviter de répéter du code dans DisplayMap 
	Entrée : 	placeId (Int) : identifiant du marker associé au point à mettre en valeur
	Sortie :	-
	*/
	/*
function callPagination(placeId) {
		// Si un paramètre a été passé, on charge les données associées
		console.log("Escapade Carbet Mobile - callPagination / placeId = " + placeId);
		
		if (lastMarkerMap == -1) {
			$('#pagination .display').attr('data-id',-100);
		}
			
		if (placeId >= 0) {
			var markerId = getMarkerIdFromPlaceId(placeId);
			$('#pagination .display').attr('data-id',markerId);
			updatePagination(markerId);
		}
		
		// Léger tempo avant de rafraichir sinon carte pas fini de redessiner
		window.setTimeout(function() { $('#map_canvas').gmap('refresh');}, 100);
	}
*/

	
	var module = {
        
        getIdCarbetFromName : getIdCarbetFromName,
                
        init: function() { 
	        
        	initLocalStorage();
        	
			displayCarbetList(getList("carbet"));
			displayPageList(getList("page"));
					
			setListeners();
			
			//displayMap(-1);
			
       },
       
       updatePagination: updatePagination
       
    };      
    return module;
    
})();