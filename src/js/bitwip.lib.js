// Version 0.2
// Lst update : 2012/05/16

BITWIP = (function() {
	
	function getUrlVars() {
	    var vars = [], hash;
	    var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
	    for(var i = 0; i < hashes.length; i++)
	    {
	        hash = hashes[i].split('=');
	        vars.push(hash[0]);
	        vars[hash[0]] = hash[1];
	    }
	    return vars;
	}
	
	/* humanDate()
	Formate la date pour l'afficher dans un format lisible
	Entrée : 	date (string) : date à convertir
	Sortie :	formatedDate (string) : date formatée et lisible
	*/
	function humanDate(date) {
		var newDate = new Date(date);
		var months = Array("Janvier", "Février", "Mars", "Avril", "Mai", "Juin", "Juillet", "Août", "Septembre", "Octobre", "Novembre", "Décembre");
		var formatedDate = newDate.getDate() + " " + months[newDate.getMonth()] + " " + newDate.getFullYear();
		return formatedDate;
	}
	
	function isPhonegap() {
		if (typeof  PhoneGap != "undefined" &&  PhoneGap != null) 
			return true;
		else
			return false;
	}
	
	function isiPhone() {
		if (!isPhonegap()) {
			return false;
		}
	    if (( device.platform == 'iPhone' ) || ( device.platform == 'iPhone Simulator')) {
	        return true;
	    } else {
	        return false;
	    }
	    return false;
	}
	
	function isiPad() {
		if (!isPhonegap()) {
			return false;
		}
	    if (( device.platform == 'iPad' ) || ( device.platform == 'iPad Simulator' )) {
	        return true;
	    } else {
	        return false;
	    }
	    return false;
	}
	
	function isAndroid() {
		if (!isPhonegap()) {
			return false;
		}
	    if (( device.platform == 'Android' )) {
	        return true;
	    } else {
	        return false;
	    }
	    return false;
	}
	
	function isLandscape() {
	    if (isiPhone()) {
	        if ($(window).width() >= 480)   {
	            return true;
	        } else {
	            return false;
	        }
	    }
	    if ($(window).width() >= 1024)   {
	        return true;
	    } else {
	        return false;
	    }
	    return false;
	}

       
    var module = {
		getUrlVars 	: getUrlVars,
		
		humanDate 	: humanDate,
		
		isPhonegap	: isPhonegap,
		isiPhone	: isiPhone,
		isiPad		: isiPad,
		isAndroid	: isAndroid,
		isLandscape	: isLandscape,
		
		         
        Storage: (function() {
            window.addEventListener("storage", function(evt) {
                //alert(evt);
            }, false);
            
            function getStorageScope(scope) {
                if (scope && (scope == "session")) {
                    return window.sessionStorage;
                } // if
                
                return window.localStorage;
            } // getStorageTarget
            
            // TODO: check for storage support here
            
            return {
            	clear: function(scope) {
            		getStorageScope(scope).clear();
            	},
            	
            	// Supprime toutes les entrées dont la clé commence par keyString
            	clearKey : function(scope,keyString) {
					if (getStorageScope(scope).length - 1) {
			            var list = [], i, key;
			            for (i = 0; i < window.localStorage.length; i++) {
			                key = window.localStorage.key(i);
			                if (key.startsWith(keyString)) {
			                    list.remove(BITWIP.Storage.get(key));
			                }
			            }
			        }
			        
			        return list;
            	},
            
                get: function(key, scope) {
                    // get the storage target
                    var value = getStorageScope(scope).getItem(key);
                    
                    // if the value looks like serialized JSON, parse it
                    return (/^(\{|\[).*(\}|\])$/).test(value) ? JSON.parse(value) : value;
                },
                
                
                set: function(key, value, scope) {
                    // if the value is an object, the stringify using JSON
                    var serializable = jQuery.isArray(value) || jQuery.isPlainObject(value);
                    var storeValue = serializable ? JSON.stringify(value) : value;
                    
                    // save the value
                    getStorageScope(scope).setItem(key, storeValue);
                },
                
                remove: function(key, scope) {
                    getStorageScope(scope).removeItem(key);
                }
                
            };
        })()
       
    };
      
    return module;
})();

// Config JQuery Mobile
$( window.document ).bind('mobileinit', function(){
	// Navigation
	$.mobile.page.prototype.options.addBackBtn = false;
	// Messages
	$.mobile.loadingMessage = "Chargement en cours…"; 
	$.mobile.loadingMessageTextVisible = false;
	$.mobile.pageLoadErrorMessage = "Oups, il y a eu un soucis lors du chargement de la page. Merci de recommencer.";
	// Transitions
	$.mobile.defaultPageTransition = "none";
	$.mobile.transitionFallbacks.slideout = "none";
	$.mobile.buttonMarkup.hoverDelay = "50";
	/*
	if (BITWIP.isPhonegap()) {
		console.log("Escapade Carbet Mobile : OS = " + device.platform);
		if (device.platform == "Android")
			$.mobile.defaultPageTransition = "fade";
		else
			$.mobile.defaultPageTransition = "slidefade";
	} else
		$.mobile.defaultPageTransition = "fade";
	*/
	
	console.log("Escapade Carbet Mobile : Paramètres JQuery Mobile intialisés");
});

// jqm.page.params.js - version 0.1
// Copyright (c) 2011, Kin Blas
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//     * Redistributions of source code must retain the above copyright
//       notice, this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above copyright
//       notice, this list of conditions and the following disclaimer in the
//       documentation and/or other materials provided with the distribution.
//     * Neither the name of the <organization> nor the
//       names of its contributors may be used to endorse or promote products
//       derived from this software without specific prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
// DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

(function( $, window, undefined ) {

	// Given a query string, convert all the name/value pairs
	// into a property/value object. If a name appears more than
	// once in a query string, the value is automatically turned
	// into an array.
	function queryStringToObject( qstr )
	{
		var result = {},
			nvPairs = ( ( qstr || "" ).replace( /^\?/, "" ).split( /&/ ) ),
			i, pair, n, v;
	
		for ( i = 0; i < nvPairs.length; i++ ) {
			var pstr = nvPairs[ i ];
			if ( pstr ) {
				pair = pstr.split( /=/ );
				n = pair[ 0 ];
				v = pair[ 1 ];
				if ( result[ n ] === undefined ) {
					result[ n ] = v;
				} else {
					if ( typeof result[ n ] !== "object" ) {
						result[ n ] = [ result[ n ] ];
					}
					result[ n ].push( v );
				}
			}
		}
	
		return result;
	}
	
	// The idea here is to listen for any pagebeforechange notifications from
	// jQuery Mobile, and then muck with the toPage and options so that query
	// params can be passed to embedded/internal pages. So for example, if a
	// changePage() request for a URL like:
	//
	//    http://mycompany.com/myapp/#page-1?foo=1&bar=2
	//
	// is made, the page that will actually get shown is:
	//
	//    http://mycompany.com/myapp/#page-1
	//
	// The browser's location will still be updated to show the original URL.
	// The query params for the embedded page are also added as a property/value
	// object on the options object. You can access it from your page notifications
	// via data.options.pageData.
	$( document ).bind( "pagebeforechange", function( e, data ) {
	
		// We only want to handle the case where we are being asked
		// to go to a page by URL, and only if that URL is referring
		// to an internal page by id.
	
		if ( typeof data.toPage === "string" ) {
			var u = $.mobile.path.parseUrl( data.toPage );
			if ( $.mobile.path.isEmbeddedPage( u ) ) {
	
				// The request is for an internal page, if the hash
				// contains query (search) params, strip them off the
				// toPage URL and then set options.dataUrl appropriately
				// so the location.hash shows the originally requested URL
				// that hash the query params in the hash.
	
				var u2 = $.mobile.path.parseUrl( u.hash.replace( /^#/, "" ) );
				if ( u2.search ) {
					if ( !data.options.dataUrl ) {
						data.options.dataUrl = data.toPage;
					}
					data.options.pageData = queryStringToObject( u2.search );
					data.toPage = u.hrefNoHash + "#" + u2.pathname;
				}
			}
		}
	});

})( jQuery, window );

$(document).bind("pagebeforechange", function( event, data ) {
    $.mobile.pageData = (data && data.options && data.options.pageData)
        ? data.options.pageData
        : null;
});

// Déclaration de la méthode startsWith()
if (typeof String.prototype.startsWith != 'function') {
  String.prototype.startsWith = function (str){
    return this.slice(0, str.length) == str;
  };
}